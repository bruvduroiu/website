module gitlab.com/bruvduroiu/website

go 1.19

require (
	github.com/bruvduroiu/hugo-maps v0.0.0-20240318065531-c9b7ad2e97e2 // indirect
	github.com/dzello/reveal-hugo v0.0.0-20240121164136-5e38035fe41f // indirect
)
