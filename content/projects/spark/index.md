---
type: project
author: Bogdan Buduroiu
title: Alai Spark
link: https://www.alaistudios.com/technology
image:
  src: cover.png
---

RAG (Retrieval-Augmented Generation) engine based on disparate media ingestion and understanding (incl. cloud platform engineering, backend engineering [*])
