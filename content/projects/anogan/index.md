---
type: project
author: Bogdan Buduroiu
title: Unsupervised Anomaly Detection using Generative-Adversarial Networks
link: /research/outlier-detection-gan
---

