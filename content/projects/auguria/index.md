---
type: project
author: Bogdan Buduroiu
title: Auguria
link: https://auguria.io
image:
  src: cover.jpeg
---
Semantic Knowledge Layer for hierarchical categorization of high-throughput log streams for SIEM systems [*]
