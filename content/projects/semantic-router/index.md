---
type: project
author: Aurelio API
title: Semantic Router
link: https://github.com/aurelio-labs/semantic-router
image:
  src: cover.png
---

Superfast AI decision making and intelligent processing of multi-modal data.
