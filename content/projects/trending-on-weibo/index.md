---
type: project
author: Bogdan Buduroiu
title: Trending on Weibo
link: https://www.trendingonweibo.com/
color: bg-purple-700
---

English-language content summarisation and automated breaking news journalism for Chinese social media
