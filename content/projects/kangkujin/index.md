---
type: project
author: Bogdan Buduroiu
title: Kangkujin
image:
  src: cover.png
---

Automatic semantic tagging of itemised receipts for Taiwan's 發票 e-Receipt system

