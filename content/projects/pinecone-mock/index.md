---
type: project
author: Bogdan Buduroiu
title: Pinecone Mock
link: https://github.com/aurelio-labs/pinecone-mock
image:
  src: cover.png
---

Lightweight, throwaway mock container for the Pinecone vector database, written in pure Golang
