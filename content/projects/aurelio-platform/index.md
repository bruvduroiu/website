---
type: project
author: Bogdan Buduroiu
title: Aurelio Platform
link: https://platform.aurelio.ai
image:
  src: aurelio-icon.png
---
Platform for extracting structured, LLM-ready text from unstructured documents
