---
type: education
university: University of Southampton
course: BSc Computer Science
date: 2018-06-01
grad: June 2018
---

First class Honours Classification with Zepler Prize for outstanding final year dissertation in ML field.

Coursework included: Advanced Machine Learning, Intelligent Systems, Computer Vision, Linear Algebra, Evolutionary Algorithms, Computational Biology, Data Structures and Distributed Computing.
