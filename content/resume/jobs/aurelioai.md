---
type: job
company: Aurelio AI
title: Staff AI Engineer
date: 2025-01-15
start: September 2023
---

- Created [Aurelio Saturn](https://platform.aurelio.ai), a B2C AI document extraction SaaS, for preparing unstructured documents for LLM use. Scaled the system to over 100 active users.

- Developed an in-house usage-based metering system handling 100,000+ usage events per second for our Saturn document extraction service.

- Provisioning and optimising GPU infrastructure (in cloud and on-prem) for serving open source Transformer and OCR models, achieving ~30% cost reduction compared to [HF Inference Endpoints](https://huggingface.co/inference-endpoints/dedicated)

- Others: vectorised BM25 retrieval algorithm with sub 100ms response time under load, developing [Semantic Router OSS library](https://github.com/aurelio-labs/semantic-router), setting up payments & auth for [Aurelio Saturn](https://platform.aurelio.ai)
