---
type: job
company: Skyscanner
title: Machine Learning Engineer
date: 2020-02-01
start: July 2017
---
- Trained machine learning models for identifying UGC detailing hotel guest experience. Extended data pipelines coalescing app events as business metrics.
