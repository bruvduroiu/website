---
type: job
company: Splyt
title: Senior DevOps Engineer
date: 2022-12-01
start: October 2021
---

- Overhauled company Infrastructure into a global, zero VPC peering, zero trust, self-service, private networked Engineering Platform. Improved dev team iteration speed while maintaining Platform team approval over core infrastructure changes

- Adopted GitOps-based CI/CD strategy, with ArgoCD at its core, providing dev teams with DevSecOps-compliant base templates (for PCIe & ISO-27001)

- Introduced Istio Service Mesh across our globally distributed private K8s clusters, for cross-service encryption using mTLS, out-of-the-box telemetry & service discovery. Zero-downtime migration onto the mesh

- Helmed creation of Incident Response guides within company, and adoption of deeper instrumentation using Datadog

- Reduced company Elasticsearch spend by 64%, using cold-node advanced index lifecycle management
