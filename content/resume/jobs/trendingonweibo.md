---
type: job
company: Trending on Weibo
title: Lead Engineer
date: 2023-05-01
start: March 2023
---

- Agentic AI journalism framework with human-in-the-loop, orchestrated using [Temporal](https://temporal.io)

- Trending topic discovery, topic clustering, agentic scraping and agentic deep-research

- 11.8K monthly page views, with 5K monthly visitors
