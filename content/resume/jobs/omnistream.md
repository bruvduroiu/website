---
type: job
company: Omnistream
title: Machine Learning Engineer
date: 2021-09-01
start: May 2020
---
- Created an idempotent Python data ingestion pipeline for FMCG retailer data, tracking data quality and change in feature importance. Improved batch job throughput by 30x. Solved supply chain issues by creating a product allocation algorithm, achieved a 40% uplift for shelf fill rates

- Hired and managed a team with two junior Data Engineers; wrote Engineering onboarding guide
