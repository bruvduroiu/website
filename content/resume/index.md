---
title: Resume
type: page
menus: none
description: Bogdan Buduroiu Resume
type: resume
stack:
  languages:
    - Python / Golang
    - Typescript / Zig
  data:
    - Temporal / Airflow
    - Apache Spark / Kafka
    - PostgreSQL / Elastic
  machine-learning:
    - LoRA / GAN
    - "LLM fine-tuning: Stable Diffusion / LLaMa"
    - PyTorch / ZML
  infra:
    - ArgoCD / Dagger
    - Terraform CDKTF
    - Istio / Envoy
    - kpt / kustomize
    - Prometheus / Jaeger
---
