---
type: blog
author: Bogdan Buduroiu
minRead: 4
title: "Setup a Remote NVIDIA AI Workbench Node using EC2"
description: How to run GPU-accelerated ML workloads with ease using NVIDIA's AI Workbench and a CUDA-enabled EC2 instance.
date: 2024-04-01
link: https://www.aurelio.ai/learn/ai-workbench-remote
tags:
- nvidia
- devops
categories:
- engineering
image:
    src: cover.png
---
