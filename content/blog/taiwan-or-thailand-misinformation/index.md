---
type: blog 
author: Bogdan Buduroiu
minRead: 4
title: "Taiwan or Thailand? A primer to gaffes, misinformation and warfare"
description: "A technical gaffe in a CNN NYE story in Taiwan caused a wider outrage on social media, and became ammunition for the more hawkish voices reporting on China."
date: 2023-01-28
draft: false
tags:
- taiwan
- misinformation
categories:
- taiwan
image:
  src: cover.png
  caption: Will Ripley reporting from Taipei on New Year's Eve 2022 in Bangkok
---

On New Year's Eve 2022, CNN ran a segment on how different Asian cities celebrate the occasion. A technical gaffe caused a wider outrage on social media, and became ammunition for the more hawkish voices reporting on China.

Correspondents Will Ripley, based out of Taipei, and Kristie Lu Stou, out of Hong Kong, were reporting on the passage into the New Year, as celebrated in Asia.

Keeping in sync with the different timezones starting from GMT+9 (Japan) and going backwards, Will and Kristie aimed to report on how different cities in Asia celebrate New Year's Eve, just as they crossed the 00:00 mark into the new year.

The plan was to have the two reporters speak about the different countries, their NYE customs, and also sprinkle in some of their own travel experiences here and there in order to maintain a light atmosphere.
In order to diffuse confusion as to why they'd be talking about Bangkok while being in Taipei, the crew was supposed to play some B-roll of each individual city's celebrations.

The story started well, and after Taipei and Hong Kong transitioned into the New Year, it was time for Bangkok to do so as well. However, technical issues got in the way, and we were left staring at Will and Kristie, instead of views of the celebrations in Bangkok.

The combination of the two reporters talking about Bangkok from different locations, Kristie asking Will about his Thailand experiences, and the missing B-roll led to an unfortunate screen grab, where Will's location was noted as "Taipei, Taiwan", while the banner at the bottom of the screen wrote "Thailand's Capital welcomes 2023", alluding that CNN would mistake Taiwan for Thailand.

Before we even got a chance to see the story, the image had already made the rounds on Twitter, causing eyerolls left and right at CNN's apparent ignorance, and the classic error of mistaking Taiwan for Thailand.

To be clear, Kristie is an American-Chinese journalist who attended Tsinghua University in Beijing, Will is a Taiwan resident himself, and Taiwan is 1 hour ahead of Thailand. There is no doubt that both of them knew the difference between Taiwan and Thailand, and a gaffe like that would've been 1 hour too late.

It wasn't long until this outrage spread to the more aggressive voices of the Chinese media apparatus, with the Spokesperson for the Chinese Ministry of Foreign Affairs, Hua Chunying, jumping at the opportunity to point out the "irony" of the situation.

{{< fig src="images/hua_chunying_nye.png" caption="Spokesperson to China's Ministry of Foreign Affairs, Hua Chunying takes to Twitter" class="object-cover w-2/3 mx-auto rounded-lg md:max-lg:w-full md:max-lg:h-full h-2/3" >}}

While this could be brushed off as yet another spicy take from a Chinese Spokesperson, à la [Chen Weihua](https://twitter.com/chenweihua/status/1334561058799964164?lang=en), I wanted to take a moment to underline how these activities are in line with the Operational Concepts of the People's Liberation Army (PLA).

> During a conflict, military campaign activities will have to be synchronized with public opinion and psychological and legal warfare activities to ensure the consistency of the narrative presented to adversaries, partners, and the larger regional and international communities.<sup>[1]</sup>


... which leads me on a detour to the Kansai Airport Evacuation story.

## The Kansai Airport Evacuation Incident

On September 4th, 2018, Typhoon Jebi hit Kansai International Airport in Osaka, Japan. The airport had to pause operations after seawater surges inundated the island, rendering the runways unusable. The situation was exacerbated when a large tanker crashed into the bridge linking the airport to the mainland, effectively stranding numerous people at the airport.

Quickly, the Chinese consulate in Osaka announced that it organised buses for Chinese nationals to be evacuated, with Chinese media outlets circulating several stories praising the consulate.

In response, Taiwanese netizens slammed the Taipei Economic and Cultural Office (TECO) in Osaka for failing to assist Taiwanese tourists in an equal manner.

As we've seen with the NYE story above, social media made quick work rumouring that Taiwanese citizens had to feign Chinese identities in order to get on the buses organised by the Chinese consulate. As expected, this only fanned the flames of the Taiwanese netizens' dissatisfaction towards the TECO.

This social media report was quickly picked up by Chinese domestic media and international news outlets, before the story could be verified.

{{< fig src="images/kansai_news.png" caption="South China Morning Post reports on the Kansai Airport story" class="object-cover" >}}

The Taiwanese Ministry of Foreign Affairs (MOFA) tried to calm down the tensions, reassuring people that an investigation is being conducted and that they are complying with the rescue protocols put forward by the Japanese authorities.

The appeals for calm from the MOFA did not quench the outrage and, on the morning of Sep 14, 2018, Su Chii-cherng (蘇啟誠), the director-general of the Osaka branch of the TECO committed suicide at his residence. The 61-year-old left behind a letter saying he was deeply pained by public criticism accusing his office of not doing enough to rescue Taiwanese tourists stranded at the airport.

The following day, the Japanese authorities debunked the Chinese envoy's claims, stating that Japanese authorities evacuated everyone from Kansai International Airport, regardless of nationality.

The Kansai International Airport and New Year's Eve stories are just two of many. Their aim is to paint a picture of Taiwan being weak and divided, lacking national identity, with a government that kowtows to states that frequently mistake it for other nations, and a population waiting to be rescued by their Chinese "compatriots".

Taiwan is undoubtedly Beijing's testing ground for psychological warfare and misinformation, with a peak of 4.9 million cyberattacks happening daily during the visit of previous U.S. House Speaker Nancy Pelosi.

This doesn't mean that Taiwan isn't squaring up to the fight: Taiwan's Ministry of Digital Affairs, helmed by Audrey Tang is putting in protections against cyberwarfare, as well as creating educational programs to combat misinformation.

However, these incidents beg reflection on the weaponisation of misinformation to weaken enemy morale, with the end goal of winning wars without firing a single bullet.

--- 

- Burke, Edmund J., et al. People's Liberation Army Operational Concepts. RAND CORP SANTA MONICA CA, 2020.<sup>[[1]](https://apps.dtic.mil/sti/citations/AD1110216)</sup>
