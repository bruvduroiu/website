---
type: blog
author: Bogdan Buduroiu
minRead: 2
title: "Using Hetzner Object Storage as Terraform S3 backend"
description: Hetzner's Object Storage service provides an S3-compatible API
date: 2025-02-27
tags:
- engineering
- devops
categories:
- engineering
image:
  src: cover.png
---

Hetzner's Object Storage is an S3-compatible service, meaning it can be used for configuring Terraform backends with.

Here's how you do that, assuming you've gone through the process of [creating a bucket](https://docs.hetzner.com/storage/object-storage/getting-started/creating-a-bucket/) as well as [generating S3 keys for it](https://docs.hetzner.com/storage/object-storage/getting-started/generating-s3-keys)


```terraform
backend "s3" {
  bucket = "your-bucket"
  endpoints = {
    s3 = "https://hel1.your-objectstorage.com" # Hetzner's endpoint
  }
  key = "your-project.tfstate" # Name of the tfstate file

  region                      = "main" # this is required, but will be skipped!
  skip_credentials_validation = true   # this will skip AWS related validation
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_requesting_account_id  = true   # skips checking STS 
  use_path_style              = true   # Ceph-S3 compatibility
  skip_s3_checksum            = true   # Ceph-S3 compatibility
}
```

Notes:

- you want to skip the mechanisms for fetching account ID from STS, as well as other S3 validation steps
- use the correct endpoint corresponding to your Hetzner Bucket's region, here `hel1.your-objectstorage.com`
- it is recommended you use `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` in your environment instead of baking these keys into your Terraform files
- skips checksums, currently needed for Ceph-S3 type services (https://github.com/hashicorp/terraform/issues/34086#issuecomment-1814188931)


```bash
≫ terraform init

Initializing the backend...

Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.
```
