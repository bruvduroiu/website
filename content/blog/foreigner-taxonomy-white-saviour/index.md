---
type: blog
author: Bogdan Buduroiu
minRead: 4
title: "Foreigner taxonomy: the White Saviour"
date: 2024-10-01
draft: true
tags:
- society
---


When doing a taxonomy of male foreigners in Asia, a lot of ink is being spilled (with good reason) on passport bros, digital nomads, and English teachers.

One archetype that doesn't get the limelight, but ironically suffers from the same level of prejudice towards locals, though through a veneer of social justice, is the White Saviour.

This post was inspired by a thread in [/r/Taiwan](/r/Taiwan), where white foreigners in Taiwan were described as low lifes with no other places to go, for which a buxiban job is the pinnacle of career achievement, and pulling the wool over the eyes of the local opposite gender is their romantic crusade.

And while this post is not here to address the veracity of those claims, I do want to look at how the moral high ground of social justice is sometimes a place from which to cast down prejudice and bigotry.

## The White Man's Burden

In 1899, on the eve of the Filipino-American War, Rudyard Kipling would publish a poem titled [The White Man’s Burden](https://en.wikipedia.org/wiki/The_White_Man%27s_Burden). The poem urges the United States to rise to meet the “civilising mission” of colonising the Philippine Islands and its people. The premise of the poem is that the white race is morally obliged to civilise and bring progress to the unwilling savage.

Probably the most glaring example of modern day White Saviour Complex was the hugely viral Kony 2012 campaign. A massively viral short documentary film produced by Invisible Children, which initially intended to bring ICC fugitive criminal Joseph Kony to justice. However, through glaring oversimplification and peddling of dangerous and patronising falsehoods, the movie raised a failed provincial politician to the status of warlord and global celebrity, rather than demystifying him as a common criminal.

Now the thing that binds our Redditors together in the same league as KONY 2012 and Rudyard Kipling’s call to colonialism is that the comment is rooted in the White Saviour Complex.

In an attempt to distance themself from “those white foreigners”, similarly to how Kipling saw it as the white man’s burden to civilise the savages, the author sees it as their white man’s burden to “protect” the Taiwanese society from “foreign dregs”, and “liberate” and Taiwanese women, regardless of the opinion of the local.

Stating that “it is a privilege and not a right” to be in Taiwan, while factually correct, is said at the expense of marginalised groups for which this statement is an actual political talking point preventing them from having and exercising rights. No white expat will be systematically oppressed due to this statement, but migrant workers fighting for labour rights might.

Stating that “Taiwanese women have the wool pulled over their eyes by the Western Allure”, besides being an infantilising comment robbing Taiwanese women of their agency, is an attempt of elevating one’s self to the status of White Saviour. However, this comes at the expense of Taiwanese women, degrading them as incapable of resisting the “Western Allure”. These comments further feed the existing levels of misogyny in Taiwanese society, fanning the flames of [ㄈㄈ尺](https://nihaositgoing.com/2020/12/29/why-ㄈㄈ尺-ccr-is-misogynistic-ethno-nationalism) misogyny, and even going as far as breathing life into political figures who co-opt these sentiments for political gain.
