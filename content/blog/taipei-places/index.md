---
type: blog
author: Bogdan Buduroiu
minRead: 2
title: Bogdan's Taipei Spots
description: What I like to do in and around Taipei
date: 2024-03-17
tags:
- taiwan
- life
categories:
- taiwan
---

Updated frequently, keep checking...

{{< map name="taipei_map" height="400px" >}}


## Grey Area

Dark, precise, stripped back beats upstairs. Lounge, chill out, fashion show in the cafe downstairs.

The aesthetic of this place is immaculate, a confluence of edgy fashion students, alternative scene and the occasional expat.


## 巢 nido

Silence. Focus. Damn good pudding.

Step into a meticulous cafe, at times even pedantic as the owner speaks in silence, and is not afraid to sternly tell patrons to do the same.

## Draft Land


The Xinyi Draft Land store is overflowing with partygoers every weekend, but their main store is often overlooked.

Intimate, cozy, with a secret room which hosts guest artists and their artwork, I love getting lost here by myself and enjoying the kinetic art pieces.

## 開門茶堂

Hard to not rate a cozy teahouse in the middle of a sleepy residential area.

## 小後苑Backyard Jr.

Taipei's Whiskey Library. My Friday 1am post-US client meeting hangout spot for suprise finds and indulgent instant noodles topped with steak.

Good company. Good whiskey. Indulgent food.

## Homey's Cafe

Feels like you're visiting your mate for a coffee and a smoke on his rental's balcony.

The cafe is filled with leftist books, posters, future and past activism campaigns.
