---
type: blog
author: Bogdan Buduroiu
minRead: 9
title: "Quick classification of Taiwan's Indigenous Weapons Programmes"
description: The unveiling of Taiwan's Hai Kun-class submarine is flexing the country's military-industrial muscles
date: 2023-09-29
draft: false
tags:
- taiwan
- military
categories:
- taiwan
image:
  src: cover.jpeg
---

This August, I had the privilege to join the first Ching Chuan Kang Air Base Annual Open Day after the COVID Pandemic. I've had an absolute blast looking at all the [ELINT](https://en.wikipedia.org/wiki/Signals_intelligence) planes, [ASW](https://en.wikipedia.org/wiki/Anti-submarine_warfare) helicopters and fighters.

The most impressive part though, was that I would walk up to an exhibit, and upon reading the information card, I'd find out it's actually indigenously produced.

So, with the unveiling of the Hai Kun-class Indigenous Defense Submarine, here's a quick rundown of (some of) Taiwan's current and future indigenous defense programmes.


## AIDC F-CK-1 Ching-kuo 經國號戰機

{{< fig src="images/IMG_6768.jpeg" caption="AIDC F-CK-1 Ching-kuo presented together with TC-1 天劍一, TC-2 天劍二, Wan Chien 萬劍 missiles, as well as an array of 20mm rounds" >}}

The AIDC F-CK-1 Ching-kuo, commonly known as "Indigenous Defense Fighter", was started off the back of the United States refusing to sell F-20 Tigersharks and F-16 Fighting Falcons to the ROCAF, after President Nixon signed the Sino-US Joint Communiqué, which severely restricted weapons sales to Taiwan.

Because of the US State Department's arms export control, many processes, from airframe design to engine and Missile R&D had to be supported by the Aerospace Industrial Development Corporation, which further increased skill transfer and Taiwan's indigenous military manufacturing capability.

This aircraft was considered for a secondary delivery vehicle during Taiwan's Nuclear Weapons Programme.

## Advanced Defense Fighter Programme

{{< fig src="images/Kf-21-e1636706059648.jpg.webp" caption="Korean KF-21 Boramae. Taiwan's ADF heavily borrows from the KF-21's design" >}}

With a current inventory made up of F-16s, Mirage 2000s, IDFs and F5s, ROCAFs configuration is unlikely to contest the airspace and prevent the PLAAF from gaining air superiority.

Therefore, in 2017, Taiwan's AIDC has announced plans to develop it's next-generation indigenous defense fighter, featuring stealth capabilities. 

Though the programme is currently estimated for testing in 2025, little has been said about the programme, though recently AIDC has asked for US help in developing the engine, avionics, control systems and so on.

It is speculated that in order to save time, the design of the fighter will be heavily based on existing platforms, such as the Korean KF-21 Boramae.

The most notable part of this aircraft is it's similarity to the Lockheed Martin F-35.

## CM-32 "Clouded Leopard" AV 雲豹裝甲車

{{< fig src="images/cm-32.jpg" caption="CM-32, 30mm cannon exhibit at Taiwan's 110th National Day. Photo by Presidential Office" >}}

Another platform which is unmissable around Taiwan, even in civilian scenarios, is the Clouded Leopar Armoured Vehicle, officially Taiwan Infantry Fighting Vehicle.

It comes in many variants, from its 30mm cannon version, a mobile-gun platform variant, and, with the future CM-32 Yunpao, a mobile 81mm mortar version.

It is estimated that up to 1,400 CM-32s may end up bein in operational service, with a Clouded Leopard II prototype expected to drop this October 2023.

In 2021, an alleged substitution of poor quaility Chinese parts affecting 326 chassis landed the senior executives in charge of the project in jail for up to 6 years.

## Missiles

{{< fig src="images/Taiwan_missiles_web.jpg" caption="Image courtesy of CSIS, Missile Defense Project" >}}

Countering a growing PLA Navy (currently, China reportedly has a 230x bigger shipbuilding capability to the United States), as well denying the PLA Air Force air superiority, Taiwan has developed a set of surface-to-surface, surface-to-air and air-to-air missiles.

The most notable missiles in the inventory of the ROCA and ROCAF are the Hsiung Feng 雄風 (Brave Wind), Sky Sword 天劍 and Cloud Peak 雲峰 missiles. They differ in use and I'll go over them quite quickly.

### Hsiung Feng Family 雄風巡弋飛彈

{{< fig src="images/IMG_6739.jpeg" caption="Yours truly posing with a Hsiung Feng Anti-Ship Missile Launcher" >}}

This family of surface-to-surface missiles are mainly designed to hit land based and naval targets, and are split between subsonic and supersonic missiles. The HF-2 and supersonic HF-3 are mainly designed to hit naval targets, while the HF-2E is built with land tagets in mind.

Extended-range HF-2E and HF-3E are currently in development, with the latter boasting a 400km range.

The range of the HF-2E missiles has made various columnists and online personalities claim that these missiles could strike China's Three Gorges Dam, causing massive floodings and killing millions. However, these claims are largely unsubstantiated, and generally against the consensus of military experts.

As with many indigenous weapons, the Hsiung Feng family was also involved in a scandal, after a January 2023 report showed that Taiwan NCSIST has sent theodolites from the HF-3 missile for maintenance, only for those theodolites to be further sent for repairs in China. While on the surface this seems as a massive intel breach, the theodolites used were readily available and sold commercially. This, however, didn't stop headlines of "Taiwan's most advanced anti-ship missiles repaired in China" from popping up in the following weeks.

### Sky Sword Family 天劍

{{< fig src="images/ss2.jpeg" caption="Air-to-air version of the Sky Sword 2" >}}

These beyond-visual-range missiles were developed by Taiwan's National Chung-Shan Institute of Science and Technology.

They come in air-to-air and surface-to-air variants


### Yun Feng 雲峰

{{< fig src="images/yf.webp"  >}}

This is one of the few Taiwanese strategic assets designed to reach targets deep in northern and central China.

The programme was developed in secret, concealing flight tests within the Hsiung Feng III test programme.

The extended range of this surface-to-surface missile has made it a controversial asset, given Taiwan's defensive posture, and especially given China's advanced air defense capabilities.


## Yushan-class landing platform dock

{{< fig src="images/yushan.jpeg" caption="ROCS Yushan docking at Tsoying Naval Yard" >}}

Planning for four ships of this type to be launched, the first one was released in April 2021, and comissioned in June 2023.

The ship carries two Black Hawk or Seahawk helicopters, and is armed with 4 Hsiung Feng-II SSMs and 32 Sky Sword SAMs. It has a range of 11,000km and a capacity of various amphibious APCs and up to 673 troops.

## Hai Kun-class submarine 海鯤級潛艦

{{< fig src="cover.jpeg" caption="Narwhal, the first Hai Kun-class IDS being inaugurated" >}}

Finally, the current day talk of the town, we have the Hai Kun-class indigenous defense submarine

Named after the mythical tale of a sea monster, the IDS programme was also started after Taiwan has failed to secure foreign-built submarines.

Released earlier this week, "Narwhal", the first instance in a batch of 8 to come has been welcomed with congratulations from President Tsai Ing-wen.

<div class="prose-quoteless">
	<blockquote>
		"Building indigenous submarines, starting from scratch, is a long and complicated journey, but today, we did it." - Tsai Ing-wen at the IDS launch event
	</blockquote>
</div>

The 230 ft long, 3,000 tonne submarine is capable of laying mines and attacking warships, and is part of Taiwan's deterrence posture, aiming to throw a monkey wrench in China's plans for a kinetic unification of Taiwan.

While questions arise about Taiwan's ability to finance it's Indigenous Submarine Programme, the consensus amongst military experts is that such a programme would be an effective deterrent against a growing PLA Navy.

## Other notable mentions

| Name | Role | Notes |
| :--- | :--: | :---: |
| [CS/MPQ-90 Bee Eye radar](https://en.wikipedia.org/wiki/CS/MPQ-90_Bee_Eye) | radar | scandal involving data forgery and fabrication of test results |
| [Kestrel anti-tank weapon](https://en.wikipedia.org/wiki/Kestrel_(rocket_launcher)) | shoulder-launched anti-tank weapon | became the Tsai Ing-wen Rocket Launcher meme |
| [Sky Horse ballistic missile](https://en.wikipedia.org/wiki/Sky_Horse) | ballistic missile | considered primary delivery method for Taiwan's previous secret nuclear weapons programme |

## Conclusion

Taiwan is a highly educated country that went through a period of rapid industrialisation following World War 2. In this context, given its tense relationship with China and exclusion from the world stage, it is not surprising that Taiwan's indigenous military-industrial complex has developed to be one of the most advanced in the world.
