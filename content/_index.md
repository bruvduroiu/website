---
type: page
title: Bogdan Buduroiu
name: Bogdan
surname: Buduroiu
tagline: Machine Learning Operations Engineer
description: Platform and Data Engineer, with a focus on Machine Learning Operations, Cloud Native Infrastructure, and Developer Experience.
images:
- "images/headshot.png"
jobs:
- company: AurelioAI
  role: "Lead MLOps Engineer"
  duration:
    from: 2023
    until: present
  image: images/aurelio.png
- company: Splyt
  role: "Senior DevOps Engineer"
  duration:
    from: 2021
    until: 2022
  image: images/splyt.png
- company: Omnistream
  role: "Machine Learning Engineer"
  duration:
    from: 2020
    until: 2021
  image: images/omnistream.png
- company: Skyscanner
  role: "Machine Learning Engineer"
  duration:
    from: 2017
    until: 2020
  image: images/skyscanner.png
projects:
- name: "Trending on Weibo"
  description: "Chinese Social Media content summarisation and translation using Large Language Models"
  image: images/trending_on_weibo.png
---

software engineer with education in deep learning and experience in platform engineering. currently @ <a href="https://aurelio.ai" class="text-pink-600 underline">aurelio.ai</a>
